package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
)

// occupancy alert posts an information to CEDA URL
func sendPost(url string) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Print(err)
	}
	defer func() {
		if resp != nil {
			if err := resp.Body.Close(); err != nil {
				fmt.Print(`could not close response body: %s`, err)
				return
			}
		}
	}()
	if resp == nil {
		fmt.Print(`response is nil`)
		return
	}
	switch resp.StatusCode {
	case http.StatusOK, http.StatusNoContent:
	default:
		fmt.Print("could not post request to %q, response status code: %d", url, resp.StatusCode)
		return
	}
	fmt.Print(`post sent`)
}

func main() {
	url := os.Getenv("URL")
	no := os.Getenv("NUMBER")
	no_int, _ := strconv.Atoi(no)
	for i := 1; i < no_int; i++ {
		go sendPost(url)
	}
}
